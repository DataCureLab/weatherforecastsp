package org.datacure.weatherforecast.mappers;

import org.datacure.weatherforecast.owclient.dto.OWResponse;

public interface WeatherMapper<T> {
    static final String TEMPERATURE = "Temperature";
    static final String HUMID = "Humid";

    T toReport(OWResponse response);
}
