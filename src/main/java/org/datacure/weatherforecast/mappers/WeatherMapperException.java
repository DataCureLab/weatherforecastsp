package org.datacure.weatherforecast.mappers;

public class WeatherMapperException extends RuntimeException {
    public WeatherMapperException(String message) {
        super(message);
    }

    public WeatherMapperException(String message, Throwable cause) {
        super(message, cause);
    }
}
