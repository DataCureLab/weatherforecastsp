package org.datacure.weatherforecast.services;

import org.datacure.weatherforecast.mappers.WeatherMapper;
import org.datacure.weatherforecast.owclient.OpenWeatherClient;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public class WeatherService {
    OpenWeatherClient owClient;

    WeatherMapper<String> textMapper;

    WeatherMapper<InputStream> pdfMapper;

    public WeatherService(OpenWeatherClient owClient, WeatherMapper<String> textMapper, WeatherMapper<InputStream> pdfMapper) {
        this.owClient = owClient;
        this.textMapper = textMapper;
        this.pdfMapper = pdfMapper;
    }

    public String getWeather(String city) {
        return textMapper.toReport(owClient.getWeather(city));
    }

    public InputStreamSource getPdf(String city) {
        var res = pdfMapper.toReport(owClient.getWeather(city));
        return new InputStreamResource(res);
    }
}
