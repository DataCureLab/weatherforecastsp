package org.datacure.weatherforecast.web.mappers;

import org.datacure.weatherforecast.owclient.exceptions.OWException;
import org.datacure.weatherforecast.owclient.exceptions.OWNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({OWNotFound.class})
    protected ResponseEntity<String> handleEntityNotFound(OWNotFound ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body(ex.getMessage());
    }

    @ExceptionHandler({OWException.class})
    protected ResponseEntity<String> handleEntityNotFound(OWException ex) {
        return ResponseEntity.internalServerError().contentType(MediaType.TEXT_PLAIN).body("Internal server error. Try later...");
    }
}
