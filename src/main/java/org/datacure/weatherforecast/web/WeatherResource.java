package org.datacure.weatherforecast.web;

import org.datacure.weatherforecast.services.WeatherService;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
public class WeatherResource {
    WeatherService weatherService;

    public WeatherResource(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping
    public String getWeather(@RequestParam("city") String city) {
        return weatherService.getWeather(city);
    }

    @GetMapping("/pdf")
    public ResponseEntity<InputStreamSource> getPdf(@RequestParam("city") String city) {
        return ResponseEntity.ok()
                .header("content-disposition", "attachment; filename = result.pdf")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(weatherService.getPdf(city));
    }
}
