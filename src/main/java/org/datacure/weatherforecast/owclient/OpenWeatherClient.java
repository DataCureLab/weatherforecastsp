package org.datacure.weatherforecast.owclient;


import org.datacure.weatherforecast.owclient.dto.OWResponse;
import org.datacure.weatherforecast.owclient.exceptions.OWException;

public interface OpenWeatherClient {
    OWResponse getWeather(String city) throws OWException;
}
