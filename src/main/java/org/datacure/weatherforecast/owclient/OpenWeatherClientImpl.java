package org.datacure.weatherforecast.owclient;


import org.datacure.weatherforecast.owclient.dto.OWResponse;
import org.datacure.weatherforecast.owclient.exceptions.OWException;
import org.datacure.weatherforecast.owclient.exceptions.OWNotFound;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class OpenWeatherClientImpl implements OpenWeatherClient {
    private static final String METRIC = "metric";
    private RestTemplate restTemplate;

    @Value("${open-weather-api.url}")
    private String url;

    @Value("${open-weather-api.appid}")
    private String appid;

    public OpenWeatherClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public OWResponse getWeather(String city) {
        try {
            var req = String.format("%s/weather?units=%s&appid=%s&q=%s", url, METRIC, appid, city);
            var res = restTemplate.getForEntity(req, OWResponse.class);
            if (HttpStatus.OK.equals(res.getStatusCode())) {
                return res.getBody();
            } else {
                throw new OWException(String.format("Other error %d - %s", res.getStatusCodeValue(), res.getStatusCode()));
            }
        } catch (HttpClientErrorException ex) {
            switch (ex.getStatusCode()) {
                case NOT_FOUND:
                    throw new OWNotFound(city);
                case UNAUTHORIZED:
                    throw new OWException("Your appid is incorrect");
                default:
                    throw new OWException(String.format("Other error %s - %s", ex.getStatusCode(), ex.getMessage()));
            }
        } catch (RestClientException ex) {
            throw new OWException("Server communication error", ex);
        }
    }
}
