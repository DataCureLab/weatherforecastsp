package org.datacure.weatherforecast.owclient.exceptions;

public class OWException extends RuntimeException {
    public OWException(String message) {
        super(message);
    }

    public OWException(String message, Throwable cause) {
        super(message, cause);
    }
}
