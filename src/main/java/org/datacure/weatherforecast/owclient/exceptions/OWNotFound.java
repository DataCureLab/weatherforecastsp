package org.datacure.weatherforecast.owclient.exceptions;

public class OWNotFound extends OWException {
    public OWNotFound(String city) {
        super(String.format("%s not found", city));
    }
}
